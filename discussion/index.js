// CRUD Operations

// Create
	// - to inser documents
db.collections.insertOne({})
db.users.insertOne(
	{
		firstName: "Jane",
		lastName:  "Doe",
		age: 21,
		contact: {
			phone: "09065878633",
			email: "amir@yahoo.com"
		},
		courses: ["CSS", "JavaScript", "Python"],
		department: "none"
	}
)

// insertMany method
db.collections.insertMany()

db.users.insertMany(
	[{
		firstName: "Jane",
		lastName:  "Doe",
		age: 21,
		contact: {
			phone: "09065878633",
			email: "amir@yahoo.com"
		},
		courses: ["CSS", "JavaScript", "Python"],
		department: "none"
	},
	{
		firstName: "Neil",
		lastName:  "Armstrong",
		age: 25,
		contact: {
			phone: "09111065878633",
			email: "neil@yahoo.com"
		},
		courses: ["CSS", "JavaScript", "Python"],
		department: "astrophysics"
	},
	]
)

// Read Operations
	// - retrieves documents from the collection.
	db.collections.find()

	// SELECT * FROM users
	db.users.find(); 

// Update Operations
	// - update a document/s
	db.collections.updateOne()
	db.collections.updateMany()

	// Adding a document to modify
	db.users.insertOne(
	{
		firstName: "Test",
		lastName:  "Test",
		age: 0,
		contact: {
			phone: "0",
			email: "test@yahoo.com"
		},
		courses: [],
		department: "none"
	}
)
	// Modifying the added document
	db.users.updateOne(
		{firstName: "Test"},
		{$set: { firstName : "bill gates", age: 65, contact: {phone: "12345678", email: "bill@gmail.com"}, courses: ["PHP", "Laravel", "HTML"], department: "operatiobs", status: "active" }})


	db.users.updateMany({"_id": ObjectId("61e7fd90ed0ddec22701c2f4")}, {$unset: {"status":"active"}})


// Delete Operations
	// - delete a document/s
	db.users.insertOne({"firstName": "Joy", "lastName": "Pague"});
	db.users.deleteOne({"firstName": "Joy"})